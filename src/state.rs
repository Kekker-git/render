#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

pub mod local;
pub mod pipeline;
pub mod render;
pub mod window;

pub use render::RenderState;
pub use local::LocalState;
pub use window::WindowState;

use window::{
    EventLoop,
    Window,
};

pub async fn init() -> (EventLoop<()>, LocalState, RenderState, Window) {
    let (event_loop, window) = WindowState::new("rmbl")
        .expect("Could not initialize WindowState");
    let render = match RenderState::new(&window).await {
        Ok(state) => state,
        Err(e) => panic!(e),
    };

    let local_state = LocalState {
        mouse_x: 0.0,
        mouse_y: 0.0,
    };
    (event_loop, local_state, render, window)
}


