use crate::state::RenderState;
use std::time::Instant;
use winit::{
    dpi::LogicalSize,
    event::{
        DeviceEvent,
        Event,
        VirtualKeyCode,
        WindowEvent,
    },
    event_loop::{
        ControlFlow,
    },
    error::OsError,
    window::{
        WindowBuilder,
    }
};
pub(crate) use winit::{
    event_loop::EventLoop,
    window::Window,
};


#[derive(Debug)]
pub struct WindowState;

impl WindowState {
    /// Constructs a new `EventsLoop` and `Window` pair.
    ///
    /// The specified title and size are used, other elements are default.
    /// ## Failure
    /// It's possible for the window creation to fail. This is unlikely.
    pub fn new<T: Into<String>>(title: T) ->
        Result<(EventLoop<()>, Window), OsError>
    {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_title(title)
            .with_inner_size(LogicalSize::new(480, 270))
            .build(&event_loop)
            .expect("Could not create window.");
        Ok((
            event_loop,
            window,
        ))
    }

    pub fn default() -> (EventLoop<()>, Window) {
        WindowState::new(
            &"Default".to_string(),
        ).expect("Could not create a window!")
    }

    pub fn run(event_loop: EventLoop<()>, mut render: RenderState, window: Window) {
        let mut delta: f32 = 0f32;
        let mut time = Instant::now();
        
        let mut forward: Option<bool> = None;
        let mut sideways: Option<bool> = None;
        let mut up: Option<bool> = None;
        let speed = 320f32;

        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;
            match event {
                Event::MainEventsCleared => window.request_redraw(),
                Event::DeviceEvent {
                    event: DeviceEvent::Key(input),
                    ..
                } => {
                    if let Some(key) = input.virtual_keycode {
                        let state: bool = if input.state == winit::event::ElementState::Pressed {
                            true
                        } else {
                            false
                        };
                        match key {
                            VirtualKeyCode::W => {
                                if state {
                                    forward = Some(true);
                                } else {
                                    forward = None;
                                }
                            },
                            VirtualKeyCode::A => {
                                if state {
                                    sideways = Some(false);
                                } else {
                                    sideways = None;
                                }
                            },
                            VirtualKeyCode::S => {
                                if state {
                                    forward = Some(false);
                                } else {
                                    forward = None;
                                }
                            },
                            VirtualKeyCode::D => {
                                if state {
                                    sideways = Some(true);
                                } else {
                                    sideways = None;
                                }
                            },
                            VirtualKeyCode::Space => {
                                if state {
                                    up = Some(true);
                                } else {
                                    up = None;
                                }
                            },
                            VirtualKeyCode::C => {
                                if state {
                                    up = Some(false);
                                } else {
                                    up = None;
                                }
                            },
                            _ => (),
                        }
                    }
                },
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => *control_flow = ControlFlow::Exit,
                Event::WindowEvent {
                    event: WindowEvent::Resized(size),
                    ..
                } => {
                    render.swapchain_descriptor.width = size.width as u32;
                    render.swapchain_descriptor.height = size.height as u32;
                    render.swapchain = render.device.create_swap_chain(&render.surface, &render.swapchain_descriptor);
                    let command_buf = render.resize();
                    if let Some(command_buf) = command_buf {
                        render.queue.submit(&[command_buf]);
                    }
                },
                Event::RedrawRequested(_) => {
                    let t = Instant::now();
                    render.render();
                    println!("Frametime: {:?}", t.elapsed().as_millis());
                },
                _ => (),
            }

            let side_view = render.camera.view.cross(render.camera.up);
            delta = time.elapsed().as_nanos() as f32 / 10e9;
            // Move camera
            if let Some(true) = forward {
                render.camera.eye += speed * delta * render.camera.view;
                println!("Moving forwards! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            } else if let Some(false) = forward {
                render.camera.eye -= speed * delta * render.camera.view;
                println!("Moving forwards! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            }

            if let Some(true) = sideways {
                render.camera.eye += speed * delta * side_view;
                println!("Moving sideways! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            } else if let Some(false) = sideways {
                render.camera.eye -= speed * delta * side_view;
                println!("Moving sideways! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            }

            if let Some(true) = up {
                render.camera.eye += speed * delta * render.camera.up;
                println!("Moving up! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            } else if let Some(false) = up {
                render.camera.eye -= speed * delta * render.camera.up;
                println!("Moving up! New camera location: {:?}", render.camera.eye);
                window.request_redraw();
            }

            time = Instant::now();
        });
    }
}

