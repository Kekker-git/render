#[derive(Debug, Clone, Copy, Default)]
pub struct LocalState {
    pub mouse_x: f64,
    pub mouse_y: f64,
}

impl LocalState {
    pub fn update_from_input(&mut self) {
        unimplemented!();
        /*
        if let Some(position) = input.new_mouse_position {
            self.mouse_x = position.0;
            self.mouse_y = position.1;
        }
        */
    }
}

