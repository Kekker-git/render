use crate::state::{
    render::Camera,
};
use glsl_to_spirv::{
    ShaderType::{
        Fragment as FRAG,
        Vertex as VERT,
    }
};
use std::{
    fs::File,
    iter,
    mem,
};
use tobj::Mesh;
use wgpu::{
    AddressMode,
    BlendDescriptor,
    BindGroup,
    BindGroupDescriptor,
    BindGroupLayoutEntry,
    BindGroupLayoutDescriptor,
    Binding,
    BindingResource,
    BindingType,
    Buffer,
    BufferAddress,
    BufferCopyView,
    BufferUsage,
    ColorStateDescriptor,
    ColorWrite,
    CommandBuffer,
    CommandEncoderDescriptor,
    //CompareFunction,
    CullMode,
    Device,
    Extent3d,
    FilterMode,
    FrontFace,
    IndexFormat,
    InputStepMode,
    Origin3d,
    PipelineLayoutDescriptor,
    PrimitiveTopology,
    ProgrammableStageDescriptor,
    RasterizationStateDescriptor,
    RenderPipeline,
    RenderPipelineDescriptor,
    SamplerDescriptor,
    ShaderStage,
    SwapChainDescriptor,
    TextureComponentType,
    TextureCopyView,
    TextureDescriptor,
    TextureDimension,
    TextureFormat,
    TextureUsage,
    TextureViewDimension,
    VertexAttributeDescriptor,
    VertexBufferDescriptor,
    VertexFormat,
    read_spirv,
};

const DEPTH_FORMAT: TextureFormat = TextureFormat::Depth32Float;

use zerocopy::{
    AsBytes, FromBytes,
};

#[repr(C)]
#[derive(AsBytes, FromBytes, Clone, Copy, Debug)]
pub struct Vertex {
    pub pos: [f32; 4],
    pub norm: [f32; 3],
    pub tex: [f32; 2],
}

impl Vertex {
    fn create_texels(size: usize) -> Vec<u8> {
        (0 .. size * size)
            .flat_map(|_| {
                iter::once(0xBA)
                    .chain(iter::once(0xDA))
                    .chain(iter::once(0x55))
                    .chain(iter::once(1))
            })
        .collect()
    }
}

pub struct Entity {
    pub vertex_buf: Buffer,
    pub index_buf: Buffer,
    pub index_count: usize,
    //pub bind_group: BindGroup,
    //pub uniform_buf: Buffer,
}

pub struct Pipeline {
    pub bind_group: Option<BindGroup>,
    pub uniform_buf: Option<Buffer>,
    pub pipeline: Option<RenderPipeline>,
    pub entities: Vec<Entity>,
    // 0: Vertex, 1: Frag
    pub shaders: Vec<String>,
}

impl Pipeline {
    pub fn new() -> Result<Self, ()> {
        let shaders = vec!("data/shader.vert.spv".to_string(), "data/shader.frag.spv".to_string());
        Ok(Pipeline {
            bind_group: None,
            uniform_buf: None,
            pipeline: None,
            entities: Vec::new(),
            shaders,
        })
    }

    pub fn build(&mut self, device: &Device, swapchain_descriptor: &SwapChainDescriptor, camera: Camera) -> Result<CommandBuffer, ()> {
        if self.entities.is_empty() {
            panic!("No entities in pipeline");
        }

        let mut init_encoder = device
            .create_command_encoder(&CommandEncoderDescriptor { todo: 0 });

        // Create the vertex and index buffers
        let vertex_size = mem::size_of::<Vertex>();

        // Create pipeline layout
        let bind_group_layout = device
            .create_bind_group_layout(&BindGroupLayoutDescriptor {
            bindings: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStage::VERTEX,
                    ty: BindingType::UniformBuffer {
                        dynamic: false,
                    },
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStage::FRAGMENT,
                    ty: BindingType::SampledTexture {
                        multisampled: false,
                        component_type: TextureComponentType::Float,
                        dimension: TextureViewDimension::D2,
                    },
                },
                BindGroupLayoutEntry {
                    binding: 2,
                    visibility: ShaderStage::FRAGMENT,
                    ty: BindingType::Sampler { comparison: false },
                },
/*                BindGroupLayoutEntry {
                    binding: 3,
                    visibility: ShaderStage::FRAGMENT,
                    ty: BindingType::UniformBuffer { dynamic: false },
                },*/
            ],
        });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
        });

        // Create the texture
        let size = 256u32;
        let texels = Vertex::create_texels(size as usize);
        let texture_extent = Extent3d {
            width: size,
            height: size,
            depth: 1,
        };
        let texture = device.create_texture(&TextureDescriptor {
            size: texture_extent,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsage::SAMPLED | TextureUsage::COPY_DST,
        });
        let texture_view = texture.create_default_view();
        let temp_buf = device
            .create_buffer_with_data(texels.as_slice(), BufferUsage::COPY_SRC);
        init_encoder.copy_buffer_to_texture(
            BufferCopyView {
                buffer: &temp_buf,
                offset: 0,
                bytes_per_row: 4 * size,
                rows_per_image: 0,
            },
            TextureCopyView {
                texture: &texture,
                mip_level: 0,
                array_layer: 0,
                origin: Origin3d::ZERO,
            },
            texture_extent,
        );

        // Create 2D texture sampler
        let sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter: FilterMode::Nearest,
            min_filter: FilterMode::Linear,
            mipmap_filter: FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: None,
        });

        // Create view matrix
        let cam_view = camera.view_matrix();
        let mx_ref: &[f32; 16] = cam_view.as_ref();
        self.uniform_buf = Some(device
            .create_buffer_with_data(
                mx_ref.as_bytes(),
                BufferUsage::UNIFORM | BufferUsage::COPY_DST,
            ));

        let cam_ref: &[f32; 3] = camera.eye.as_ref();
        let camera_buf = device
            .create_buffer_with_data(
                cam_ref.as_bytes(),
                BufferUsage::UNIFORM | BufferUsage::COPY_DST,
                );

        // Create bind group
        self.bind_group = Some(device.create_bind_group(&BindGroupDescriptor {
            layout: &bind_group_layout,
            bindings: &[
                Binding {
                    binding: 0,
                    resource: BindingResource::Buffer {
                        buffer: &self.uniform_buf.as_ref().unwrap(),
                        range: 0 .. 64,
                    },
                },
                Binding {
                    binding: 1,
                    resource: BindingResource::TextureView(&texture_view),
                },
                Binding {
                    binding: 2,
                    resource: BindingResource::Sampler(&sampler),
                },
                /*Binding {
                    binding: 3,
                    resource: BindingResource::Buffer {
                        buffer: &camera_buf,
                        range: 0..3,
                    }
                }*/
            ],
        }));

        // Create the render pipeline
        let vs_bytes = read_spirv(File::open(&self.shaders[0]).expect("Could not load vert shader"))
            .expect("Could not read vertex shader.");
        let fs_bytes = read_spirv(File::open(&self.shaders[1]).expect("Could not load frag shader"))
            .expect("Could not read fragment shader.");
        let vs_module = device.create_shader_module(&vs_bytes);
        let fs_module = device.create_shader_module(&fs_bytes);

        self.pipeline = Some(device.create_render_pipeline(&RenderPipelineDescriptor {
            layout: &pipeline_layout,
            vertex_stage: ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: Some(RasterizationStateDescriptor {
                front_face: FrontFace::Ccw,
                cull_mode: CullMode::Back,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
            }),
            primitive_topology: PrimitiveTopology::TriangleList,
            color_states: &[ColorStateDescriptor {
                format: swapchain_descriptor.format,
                color_blend: BlendDescriptor::REPLACE,
                alpha_blend: BlendDescriptor::REPLACE,
                write_mask: ColorWrite::ALL,
            }],
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask: 0,
                stencil_write_mask: 0,
            }),
            index_format: IndexFormat::Uint16,
            vertex_buffers: &[VertexBufferDescriptor {
                stride: vertex_size as BufferAddress,
                step_mode: InputStepMode::Vertex,
                attributes: &[
                    VertexAttributeDescriptor {
                        format: VertexFormat::Float4,
                        offset: 0,
                        shader_location: 0,
                    },
                    VertexAttributeDescriptor {
                        format: VertexFormat::Float3,
                        offset: 4 * mem::size_of::<f32>() as u64,
                        shader_location: 1,
                    },
                    VertexAttributeDescriptor {
                        format: VertexFormat::Float2,
                        offset: 4 * mem::size_of::<f32>() as u64 +
                                3 * mem::size_of::<f32>() as u64,
                        shader_location: 2,
                    },
                ],
            }],
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        }));

        Ok(init_encoder.finish())
    }
}
